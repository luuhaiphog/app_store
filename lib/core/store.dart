import 'package:app_store/models/cart.dart';
import 'package:app_store/models/catalog.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:vxstate/vxstate.dart';

class Store extends VxStore {
  CatalogModel catalog;
  CartModel cart;

  Store(){
    catalog = CatalogModel();
    cart = CartModel();
    cart.catalog = catalog;
  }
}