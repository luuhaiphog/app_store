import 'package:app_store/models/catalog.dart';
import 'package:app_store/utils/routes.dart';
import 'package:app_store/widgets/home_widgets/add_to_cart.dart';
import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';


class HomeDetailPage extends StatelessWidget {
  final Item catalog;

  const HomeDetailPage({Key key, this.catalog})
      : assert(catalog != null),
      super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
      ),
      backgroundColor: context.canvasColor,
      bottomNavigationBar: Container(
        color: context.cardColor,
        child: ButtonBar(
          alignment: MainAxisAlignment.spaceBetween,
          buttonPadding: Vx.mH8,
          children: [
            '\$${catalog.price}'.text.bold.xl3.red800.make(),
            ElevatedButton(
                onPressed: () => Navigator.pushNamed(context, Routes.cartRoute),
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(context.theme.buttonColor),
                    shape: MaterialStateProperty.all(StadiumBorder())
                ),

                child: AddToCart(catalog: catalog,).wh(100, 50)
            )],
        ).px24().py12(),
      ),
      body: SafeArea(
        bottom: false,
        child: Column(
          children: [
            Hero(
              tag: Key(catalog.id.toString()),
              child: Image.network(catalog.image),
            ).h24(context).p8(),
            Expanded(
                child: VxArc(
                  height: 30.0,
                  arcType: VxArcType.CONVEY,
                  edge: VxEdge.TOP,
                  child: Container(
                    width: context.screenWidth,
                    color: context.cardColor,
                    child: Column(
                      children: [
                        catalog.name.text.xl3.color(context.accentColor).bold.make(),
                        catalog.desc.text.xl.color(context.accentColor).make(),
                        10.heightBox,
                        'Đừng tin ai quá nhiều. Điều đó sẽ làm bạn thất vọng. Hãy yêu thương bản thân bạn nhiều hơn nữa nhé. Mãi yêu.'.text.color(Colors.black38).make().p8()
                      ],
                    ).py64(),
                  ),
                )
            )
          ],
        ),
      ),
    );
  }
}
