import 'dart:ui';

import 'package:app_store/utils/routes.dart';
import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';

class LoginPage extends StatefulWidget {
  
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String name = '';
  bool changeButton = false;

  final _formKey = GlobalKey<FormState>();
  movetoHome(BuildContext context) async {
    if(_formKey.currentState.validate()) {
      setState(() {
        changeButton = true;
      });
      await Future.delayed(Duration(seconds: 1));
      await context.vxNav.push(Uri.parse(Routes.homeRoute));
      setState(() {
        changeButton = false;
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Material(
      color: context.canvasColor,
      child: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Image.asset(
                'assets/images/undraw_Mobile_login_re_9ntv.png',
                fit: BoxFit.cover,
                height: 300,
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                'Chào chúng mày tao là $name',
                style: TextStyle(
                  fontSize: 23,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 32.0),
                child: Column(
                  children: [
                    TextFormField(
                      decoration: InputDecoration(
                        hintText: 'Nhập tài khoản:',
                        labelText: 'Tài khoản',
                      ),
                      validator: (value) {
                        if(value.isEmpty){
                          return 'Tên đăng nhập không được để trống';
                        }
                        return null;
                      },
                      onChanged: (value){
                        name = value;
                        setState(() {

                        });
                      },
                    ),
                    TextFormField(
                      obscureText: true,
                      decoration: InputDecoration(
                        hintText: 'Nhập mật khẩu:',
                        labelText: 'Mật khẩu',
                      ),
                      validator: (value) {
                        if(value.isEmpty){
                          return 'Mật khẩu không được để trống';
                        }
                        if(value.length < 6){
                          return 'Mật khẩu không được ngắn hơn 6 ký tự';
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 40.0,
                    ),
                    Material(
                      color: context.theme.buttonColor,
                      borderRadius: BorderRadius.circular(
                          changeButton ? 50 : 8
                      ),
                      child: InkWell(
                        splashColor: Colors.red,
                        onTap: () => movetoHome(context),
                        child: AnimatedContainer(
                          duration: Duration(seconds: 1),
                          width: changeButton ? 50 : 150,
                          height: 50,
                          alignment: Alignment.center,
                          child: changeButton ? Icon(
                            Icons.done,
                            color: Colors.white,
                          ):
                          Text(
                            'Đăng nhập',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 18
                            ),
                          ),
                          // decoration: BoxDecoration(
                          //   color: Colors.deepPurple,
                          // ),
                        ),
                      ),
                    )
                    // ElevatedButton(
                    //   onPressed: (){
                    //     // print('mẹ mày mệt vãi lồn');
                    //     Navigator.of(context).pushNamed(Routes.homeRoute);
                    //   },
                    //   child: Text('Đăng nhập'),
                    //   style: TextButton.styleFrom(minimumSize: Size(150,40)),
                    // ),
                  ],
                ),
              )
            ],
          ),
        ),
      )
    );
  }
}