import 'dart:convert';

import 'package:app_store/core/store.dart';
import 'package:app_store/models/cart.dart';
import 'package:app_store/models/catalog.dart';
import 'package:app_store/utils/routes.dart';
import 'package:app_store/widgets/home_widgets/header_bar.dart';
import 'package:app_store/widgets/home_widgets/header_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:http/http.dart' as http;


class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final url = "https://api.jsonbin.io/b/604dbddb683e7e079c4eefd3";

  String textList = '';
  loadText() async{
    String responseText;
    responseText = await rootBundle.loadString('assets/files/product.txt');
    setState(() {
      textList = responseText;
    });
  }

  loadData() async {
    await Future.delayed(Duration(seconds: 2));

    // test http
    // final myData = await http.get(Uri.parse(url));
    // final httpJson = myData.body;

    // test data Json
    final myData = await rootBundle.loadString('assets/files/catalog.json');
    final decodeData = jsonDecode(myData);
    var productData = decodeData['products'];
    CatalogModel.items = List.from(productData)
        .map<Item>((item) => Item.fromMap(item))
        .toList();
    setState(() {});
  }
  @override
  void initState() {
    loadText();
    loadData();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final _cart = (VxState.store as Store).cart;
    // final dummyList = List.generate(10, (index) => CatalogModel.items[0]);
    // TODO: implement build
    return Scaffold(
      backgroundColor: context.theme.cardColor,
      floatingActionButton: VxBuilder(
        mutations: {AddMutaion,RemoveMutaion},
        builder: (context,_,status) => FloatingActionButton(
          backgroundColor: context.theme.buttonColor,
          // onPressed: () => Navigator.pushNamed(context, Routes.cartRoute),
          onPressed: () => context.vxNav.push(Uri.parse(Routes.cartRoute)),
          child: Icon(
            CupertinoIcons.cart,
            color: Colors.white,
          ),
        ).badge(color: Vx.gray500, size: 22, count: _cart.item.length),
      ),
      body: SafeArea(
        child: Container(
          padding: Vx.m32,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CatalogHeader(),
              if(CatalogModel.items != null && CatalogModel.items.isNotEmpty)
                CatalogList().expand()
              else
                  CircularProgressIndicator().centered().py20().expand(),
            ],
          ),
        ),
      )
    );
  }
}



