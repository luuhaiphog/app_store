import 'package:app_store/core/store.dart';
import 'package:app_store/models/cart.dart';
import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';

class Cart extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: context.canvasColor,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: 'Giỏ Hàng'.text.make(),
      ),
      body: Column(
        children: [
          _CartList().p32().expand(),
          Divider(),
          _CartTotal(),
        ],
      ),
    );
  }
}

class _CartTotal extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final CartModel _cart = (VxState.store as Store).cart;
    return SizedBox(
      height: 120,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          VxBuilder(
            builder: (context, _, status){
              return '\$${_cart.totalPrice}'.text.xl5.color(context.theme.accentColor).make();
            },
            mutations: {RemoveMutaion},
          ),
          30.widthBox,
          ElevatedButton(
              onPressed: () {
                ScaffoldMessenger.of(context).showSnackBar
                  (
                    SnackBar(
                      content: 'Mua thì mua không hỡ trợ dâu'.text.make()
                    )
                  );
              },
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(context.theme.buttonColor)
            ),
              child: 'Mua'.text.make(),
          ).w32(context),
        ],
      ),
    );
  }
}

class _CartList extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    VxState.watch(context, on: [RemoveMutaion]);
    final CartModel _cart = (VxState.store as Store).cart;
    return _cart.item.isEmpty? 'Giỏ không có hàng vui lòng mua'.text.xl.makeCentered()
        : ListView.builder(
        itemCount: _cart.item?.length,
        itemBuilder: (context, index) => ListTile(
          leading: Icon(Icons.done),
          trailing: IconButton(
            icon: Icon(
                Icons.remove_circle_outline
            ),
            onPressed: () => RemoveMutaion(_cart.item[index]),
              // setState(() {});,
          ),

          title: _cart.item[index].name.text.make(),
        ),
    );
  }
}
