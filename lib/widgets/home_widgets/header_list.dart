import 'package:app_store/models/catalog.dart';
import 'package:app_store/utils/routes.dart';
import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';

import 'add_to_cart.dart';


class CatalogList extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        itemCount: CatalogModel.items.length,
        itemBuilder: (context ,index) {
          final catalog = CatalogModel.items[index];
          return InkWell(
            onTap: () => context.vxNav.push(
                Uri(
                    path :Routes.homeDetailsRoute,
                    queryParameters: {'id': catalog.id.toString()}
                ),
                params: catalog
            ),
            child: CatalogItem(catalog: catalog),
          );
        },
    );
  }
}

class CatalogItem extends StatelessWidget {
  final Item catalog;

  const CatalogItem({Key key, this.catalog})
      : assert(catalog != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return VxBox(
        child: Row(
          children: [
            Hero(
              tag: Key(catalog.id.toString()),
                child: CatalogImage(
                    image: catalog.image
                )
            ),
            Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    catalog.name.text.lg.color(context.accentColor).bold.make(),
                    catalog.desc.text.color(context.accentColor).make(),
                    6.heightBox,
                    ButtonBar(
                      alignment: MainAxisAlignment.spaceBetween,
                      buttonPadding: Vx.mH8,
                      children: [
                        '\$${catalog.price}'.text.bold.xl.make(),
                        AddToCart(catalog: catalog)
                      ],
                    ).pOnly(right: 8.0)
                  ],
                )
            ),
          ],
        )
    ).color(context.cardColor).rounded.square(140).make().py16();
  }
}

class CatalogImage extends StatelessWidget {
  final String image;

  const CatalogImage({Key key, this.image}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image.network(
        image
    ).box.roundedLg.p16.color(context.canvasColor).make().p16();
  }
}


