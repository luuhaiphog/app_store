import 'package:app_store/core/store.dart';
import 'package:app_store/models/cart.dart';
import 'package:app_store/models/catalog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';


class AddToCart extends StatelessWidget {
  final Item catalog;
  AddToCart({
    Key key, this.catalog
  }): super(key: key);

  final cart = CartModel();
  @override
  Widget build(BuildContext context) {
    VxState.watch(context, on: [AddMutaion]);
    final CartModel cart = (VxState.store as Store).cart;
    bool add_cart = cart.item.contains(catalog) ?? false;
    return ElevatedButton(
        onPressed: (){
          if(!add_cart){
            AddMutaion(catalog);
          }
        },
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(context.theme.buttonColor),
            shape: MaterialStateProperty.all(StadiumBorder())
        ),
        child: add_cart? Icon(Icons.done) : Icon(CupertinoIcons.cart_badge_plus)
    );
  }
}
