import 'package:app_store/models/catalog.dart';
import 'package:flutter/material.dart';
 class ItemWidget extends StatelessWidget{
   final Item item;

  const ItemWidget({Key key, this.item})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Card(
      shape: StadiumBorder(),
      child: ListTile(
        onTap: () {
          print('${item.name} đã được chọn');
        },
        leading: Image.network(item.image),
        title: Text(item.name),
        subtitle: Text(item.desc),
        trailing: Text('\$${item.price}',
          textScaleFactor: 1.5,
          style: TextStyle(
            color: Colors.deepPurple,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
 }