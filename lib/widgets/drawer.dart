import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final imagesUrl = 'https://instagram.fhan3-2.fna.fbcdn.net/v/t51.2885-19/s320x320/235561525_107295948322505_1041009818191907978_n.jpg?_nc_ht=instagram.fhan3-2.fna.fbcdn.net&_nc_ohc=_ttDvC3rvlsAX98UeZF&edm=ABfd0MgBAAAA&ccb=7-4&oh=a82566376f047f5e152f89139b5129f2&oe=612A72CC&_nc_sid=7bff83';
    // TODO: implement build
    return Drawer(
      child: Container(
        color: Colors.deepPurple,
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              padding: EdgeInsets.zero,
                child: UserAccountsDrawerHeader(
                  margin: EdgeInsets.zero,
                  decoration: BoxDecoration(
                    color: Colors.deepPurple,
                  ),
                  accountName: Text('Lưu Hải Phong', style: TextStyle(color: Colors.white)),
                  accountEmail: Text('ldxphong12c1tantrao@gmail.com',style: TextStyle(color: Colors.white)),
                  currentAccountPicture: CircleAvatar(
                    backgroundImage: NetworkImage(imagesUrl),
                  ),
                ),
            ),
            ListTile(
              leading: Icon(
                  CupertinoIcons.home,
                color: Colors.white,
              ),
              title: Text(
                'Trang chủ',
                textScaleFactor: 1.1,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
            ListTile(
              leading: Icon(
                  CupertinoIcons.profile_circled,
                color: Colors.white,
              ),
              title: Text(
                'Cá nhân',
                textScaleFactor: 1.1,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
            ListTile(
              leading: Icon(
                  CupertinoIcons.mail,
                color: Colors.white,
              ),
              title: Text(
                'Email',
                textScaleFactor: 1.1,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

}