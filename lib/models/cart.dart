import 'package:app_store/core/store.dart';
import 'package:app_store/models/catalog.dart';
import 'package:velocity_x/velocity_x.dart';

class CartModel{
  static final cartModel = CartModel.internal();
  CartModel.internal();
  factory CartModel() => cartModel;

  CatalogModel _catalog;
  //Collection của Id
  final List<int> _itemId = [];
  // query lấy item sản phầm trong list catalog
  CatalogModel get catalog => _catalog;
  set catalog(CatalogModel newCatalog){
    assert(newCatalog != null);
    _catalog = newCatalog;
  }
  // lấy giá trị item vào giỏ hàng
  List<Item> get item => _itemId.map((id) => _catalog.getById(id)).toList();
  // Tính tiền
  num get totalPrice => item.fold(0, (total, current) => total + (current.price));

}
// Thêm
class AddMutaion extends VxMutation<Store>{
  final Item item;
  AddMutaion(this.item);
  @override
  perform() {
    // TODO: implement perform
    store.cart._itemId.add(item.id);
  }

// Xóa
}class RemoveMutaion extends VxMutation<Store>{
  final Item item;
  RemoveMutaion(this.item);
  @override
  perform() {
    // TODO: implement perform
    store.cart._itemId.remove(item.id);
  }
}