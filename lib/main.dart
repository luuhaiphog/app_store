import 'package:app_store/pages/cart_page.dart';
import 'package:app_store/pages/home_detail_page.dart';
import 'package:app_store/pages/login_pages.dart';
import 'package:app_store/utils/routes.dart';
import 'package:app_store/widgets/themes.dart';
import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:url_strategy/url_strategy.dart';


import 'core/store.dart';
import 'pages/home_page.dart';

void main() {
  setPathUrlStrategy();
  runApp(VxState(store: Store(),child: MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
      return MaterialApp.router(
        // home: HomePage(),
        themeMode: ThemeMode.system,
        theme: MyTheme.lightTheme(context),
        darkTheme: MyTheme.darkTheme(context),
        debugShowCheckedModeBanner: false,
        routeInformationParser: VxInformationParser(),
        routerDelegate: VxNavigator(
          routes: {
            '/' : (uri, param) => MaterialPage(child: LoginPage()),
            Routes.homeRoute: (uri, param) => MaterialPage(child:HomePage()),
            Routes.loginRoute: (uri, param) => MaterialPage(child:LoginPage()),
            Routes.homeDetailsRoute: (uri, param){
              final catalog = (VxState.store as Store).catalog.getById(int.parse(uri.queryParameters['id']));
              return MaterialPage(
                child:HomeDetailPage(
                  catalog: catalog,
                )
              );
            },
            Routes.cartRoute: (uri, param) => MaterialPage(child:Cart()),
            }
        ),
        // initialRoute: Routes.homeRoute,

      );

  }
}